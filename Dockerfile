FROM openjdk:17-oracle AS builder
ENV APP_HOME=/app/
WORKDIR $APP_HOME
COPY build.gradle.kts settings.gradle.kts gradlew $APP_HOME
COPY gradle $APP_HOME/gradle/
RUN microdnf install findutils
RUN ./gradlew bootJar || return 0
COPY . .
RUN ./gradlew bootJar

FROM openjdk:17-oracle
ENV APP_HOME=/app/
WORKDIR $APP_HOME
COPY --from=builder $APP_HOME/build/libs/simple-dockerized-server-1.0.jar .
COPY build.gradle.kts settings.gradle.kts gradlew $APP_HOME
COPY gradle $APP_HOME/gradle/
RUN microdnf install findutils

EXPOSE 8080
ENTRYPOINT ["java", "-jar", "/app/simple-dockerized-server-1.0.jar"]
