package dev.iskanred.simpledockerizedserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimpleDockerizedServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SimpleDockerizedServerApplication.class, args);
	}

}
